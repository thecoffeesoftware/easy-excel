package com.thecoffee.easyexcel.it;

import com.github.database.rider.core.api.dataset.DataSet;
import com.thecoffee.easyexcel.it.database.Employee;
import com.thecoffee.easyexcel.it.database.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;

public class EasyExcelIT extends SpringBootDBRiderBaseTest {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Test
	void databaseIntegrationTest() {
		//given
		Employee employee = Employee.builder().age(222).name("Houston Jones").build();

		//when
		employee = employeeRepository.save(employee);

		//then
		then(employee.getId()).isNotNull();
	}

	@Test
	@DataSet(value = "datasets/SimpleTestData.json", cleanAfter = true)
	void dbRiderIntegrationTest() {
		//given
		List<Employee> employeeList;

		//when
		employeeList = employeeRepository.findAll();

		//then
		then(employeeList).hasSizeGreaterThan(1);
	}

}
