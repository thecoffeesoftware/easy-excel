package com.thecoffee.easyexcel.it;

import com.github.database.rider.core.api.dataset.DataSet;
import com.thecoffee.easyexcel.factory.ExcelFactory;
import com.thecoffee.easyexcel.it.config.TestExcelConfig;
import com.thecoffee.easyexcel.it.data.DatabaseExcelWorkbook;
import com.thecoffee.easyexcel.it.data.DatabaseExcelWorksheet;
import com.thecoffee.easyexcel.it.database.Employee;
import com.thecoffee.easyexcel.it.database.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

public class DatabaseSourceIT extends SpringBootDBRiderBaseTest {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Test
	@DataSet(value = "datasets/SimpleTestData.json", cleanAfter = true)
	void dbRiderIntegrationTest() {
		//given
		List<Employee> employeeList = employeeRepository.findAll();
		List<Employee> elderEmployeeList = employeeRepository.findByAgeAfter(399);
		ExcelFactory excelFactory = new ExcelFactory();
		DatabaseExcelWorkbook workbook = DatabaseExcelWorkbook.builder()
				.dataSheet(DatabaseExcelWorksheet.builder()
						.employeeRows(Collections.singletonList(Employee.builder()
								.age(20)
								.name("Shai Hulud").build()))
						.build())
				.summarySheet(DatabaseExcelWorksheet.builder()
						.employeeRows(elderEmployeeList)
						.build())
				.reportSheet(DatabaseExcelWorksheet.builder()
						.employeeRows(employeeList)
						.build())
				.build();

		//when
		Throwable throwable = catchThrowable(() -> excelFactory.create(new TestExcelConfig(), workbook));

		//then
		then(throwable).isNull();
	}

}
