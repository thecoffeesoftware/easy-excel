package com.thecoffee.easyexcel.it.database;

import com.thecoffee.easyexcel.workbook.Cell;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

	@Id
	@GeneratedValue
	private UUID id;

	private String name;

	@Cell(headerName = "Years of experience")
	private int age;

	@Builder.Default
	private OffsetDateTime birthDateTime = OffsetDateTime.now();

	@Builder.Default
	private LocalDate comingOfAgeRitual = LocalDate.now();
}
