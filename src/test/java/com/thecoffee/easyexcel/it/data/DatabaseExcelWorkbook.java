package com.thecoffee.easyexcel.it.data;

import com.thecoffee.easyexcel.workbook.Workbook;
import com.thecoffee.easyexcel.workbook.Worksheet;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Workbook
public class DatabaseExcelWorkbook {

	@Worksheet
	private DatabaseExcelWorksheet dataSheet;

	@Worksheet(name = "dataSheet")
	private DatabaseExcelWorksheet summarySheet;

	@Worksheet
	private DatabaseExcelWorksheet reportSheet;


}
