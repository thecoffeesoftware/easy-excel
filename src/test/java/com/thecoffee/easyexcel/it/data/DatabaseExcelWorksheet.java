package com.thecoffee.easyexcel.it.data;

import com.thecoffee.easyexcel.it.database.Employee;
import com.thecoffee.easyexcel.workbook.Row;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class DatabaseExcelWorksheet {

	@Row
	List<Employee> employeeRows;

}
