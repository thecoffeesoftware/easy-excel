package com.thecoffee.easyexcel.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class TestSpringBootApplication {
	public static void main(String[] args) {
		SpringApplication.run(TestSpringBootApplication.class, args);
	}
}
