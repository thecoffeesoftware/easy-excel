package com.thecoffee.easyexcel.it.config;

import com.thecoffee.easyexcel.factory.DefaultExcelConfig;

public class TestExcelConfig extends DefaultExcelConfig {

	@Override
	public String reportPackage() {
		return "./report";
	}
}
