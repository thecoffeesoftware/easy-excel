package com.thecoffee.easyexcel.unit.data;

import com.thecoffee.easyexcel.workbook.Workbook;
import com.thecoffee.easyexcel.workbook.Worksheet;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@Workbook
public class SimpleExcelWorkbook {

	@Worksheet
	private SimpleExcelWorksheet dataSheet;

	@Worksheet(name = "dataSheet")
	private SimpleExcelWorksheet summarySheet;

	@Worksheet
	private SimpleExcelWorksheet reportSheet;


}
