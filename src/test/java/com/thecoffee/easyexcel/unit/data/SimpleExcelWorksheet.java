package com.thecoffee.easyexcel.unit.data;

import com.thecoffee.easyexcel.workbook.Row;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class SimpleExcelWorksheet {

	@Row
	List<Employee> employeeRows;

}
