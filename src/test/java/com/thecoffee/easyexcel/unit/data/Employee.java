package com.thecoffee.easyexcel.unit.data;

import com.thecoffee.easyexcel.workbook.Cell;
import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;

@Builder
@Data
public class Employee {

	@Cell
	private String name;

	private int age;

	@Builder.Default
	private OffsetDateTime birthDateTime = OffsetDateTime.now();

}
