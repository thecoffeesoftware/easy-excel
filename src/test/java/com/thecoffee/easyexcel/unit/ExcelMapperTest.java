package com.thecoffee.easyexcel.unit;

import com.thecoffee.easyexcel.factory.EasyExcelException;
import com.thecoffee.easyexcel.factory.ExcelFactory;
import com.thecoffee.easyexcel.it.config.TestExcelConfig;
import com.thecoffee.easyexcel.unit.data.Employee;
import com.thecoffee.easyexcel.unit.data.SimpleExcelWorkbook;
import com.thecoffee.easyexcel.unit.data.SimpleExcelWorksheet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

public class ExcelMapperTest {

	@Test
	void createSimpleWorkbook() {
		//given
		ExcelFactory excelFactory = new ExcelFactory();
		SimpleExcelWorkbook workbook = SimpleExcelWorkbook.builder()
				.dataSheet(SimpleExcelWorksheet.builder()
						.employeeRows(Collections.singletonList(Employee.builder()
								.age(2)
								.name("John Wick").build()))
						.build())
				.summarySheet(SimpleExcelWorksheet.builder()
						.employeeRows(Collections.singletonList(Employee.builder()
								.age(200)
								.name("Wade Wilson").build()))
						.build())
				.reportSheet(SimpleExcelWorksheet.builder()
						.employeeRows(Arrays.asList(
								Employee.builder()
										.age(12)
										.name("Mike Tyson")
										.build(),
								Employee.builder()
										.age(72)
										.name("Arthur Pendragon")
										.build()))
						.build())
				.build();

		//when
		Throwable throwable = catchThrowable(() -> excelFactory.create(new TestExcelConfig(), workbook));

		//then
		then(throwable).isNull();
	}

	@Test
	void createFromNullWorkbook() {
		//given
		ExcelFactory excelFactory = new ExcelFactory();

		//when
		Throwable throwable = catchThrowable(() -> excelFactory.create((Object) null));

		//then
		then(throwable).isInstanceOf(EasyExcelException.class);
	}

	@Test
	void createWithNullConfig() {
		//given
		ExcelFactory excelFactory = new ExcelFactory();

		//when
		Throwable throwable = catchThrowable(() -> excelFactory.create(null, SimpleExcelWorkbook.builder().build()));

		//then
		then(throwable).isInstanceOf(EasyExcelException.class);
	}

}
