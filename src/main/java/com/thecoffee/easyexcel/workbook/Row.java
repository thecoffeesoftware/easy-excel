package com.thecoffee.easyexcel.workbook;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface Row {

	int order() default 0;

	String[] transientInWorkbooks() default {};

	String[] transientInWorksheets() default {};

	int blockNum() default 0;

	int rowOffset() default 0;

	int columnOffset() default 0;

	boolean createHeader() default true;
}
