package com.thecoffee.easyexcel.factory;

import com.thecoffee.easyexcel.generator.ExcelGenerator;

public class DefaultExcelConfig implements ExcelConfig {

	protected static DefaultExcelConfig instance;

	protected DefaultExcelConfig() {
	}

	public static DefaultExcelConfig getInstance() {
		if (instance == null) {
			synchronized (ExcelGenerator.class) {
				if (instance == null) {
					instance = new DefaultExcelConfig();
				}
			}
		}
		return instance;
	}

	@Override
	public boolean includeElementsWithoutAnnotations() {
		return true;
	}

	@Override
	public Boolean useStreaming() {
		return null;
	}

	@Override
	public String reportPackage() {
		return null;
	}

	@Override
	public boolean handleElementsWithoutAnnotationsAsRows() {
		return true;
	}

	@Override
	public boolean createHeadersForElementsWithoutAnnotations() {
		return true;
	}
}
