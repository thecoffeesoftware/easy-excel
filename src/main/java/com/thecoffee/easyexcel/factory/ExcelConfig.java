package com.thecoffee.easyexcel.factory;

public interface ExcelConfig {
	boolean includeElementsWithoutAnnotations();

	Boolean useStreaming();

	String reportPackage();

	boolean handleElementsWithoutAnnotationsAsRows();

	boolean createHeadersForElementsWithoutAnnotations();
}
