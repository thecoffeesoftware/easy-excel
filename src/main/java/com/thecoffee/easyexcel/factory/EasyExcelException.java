package com.thecoffee.easyexcel.factory;

public final class EasyExcelException extends RuntimeException {
	EasyExcelException() {
		super();
	}

	EasyExcelException(String msg) {
		super(msg);
	}

	EasyExcelException(Exception e) {
		super(e);
	}
}
