package com.thecoffee.easyexcel.factory;

import com.thecoffee.easyexcel.generator.ExcelGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class ExcelFactory {

	private final Logger logger = LoggerFactory.getLogger(ExcelFactory.class);

	private final ExcelGenerator excelGenerator = ExcelGenerator.getInstance();
	private ExcelConfig excelConfig;

	public ExcelFactory() {
		excelConfig = null;
	}

	public ExcelFactory(ExcelConfig excelConfig) {
		this.excelConfig = excelConfig;
	}

	public void create(Object workbook) {
		try {
			if (excelConfig == null) {
				excelGenerator.generate(workbook);
			} else {
				excelGenerator.generate(excelConfig, workbook);
			}
		} catch (Exception e) {
			logger.error("Error during excel generation!", e);
			throw new EasyExcelException(e);
		}
	}

	public void create(ExcelConfig excelConfig, Object workbook) {
		try {
			if (excelConfig == null) {
				throw new ExcelConfigNullException("Excel config cannot be null!");
			} else {
				excelGenerator.generate(excelConfig, workbook);
			}
		} catch (Exception e) {
			logger.error("Error during excel generation!", e);
			throw new EasyExcelException(e);
		}
	}

	public void create(List<Object> workbooks) {
		try {
			workbooks.forEach(excelGenerator::generate);
		} catch (Exception e) {
			logger.error("Error during excel generation!", e);
			throw new EasyExcelException(e);
		}
	}

	public void create(ExcelConfig excelConfig, List<Object> workbooks) {
		try {
			workbooks.forEach(wb -> create(excelConfig, wb));
		} catch (Exception e) {
			logger.error("Error during excel generation!", e);
			throw new EasyExcelException(e);
		}
	}

	public void create(Map<ExcelConfig, Object> configWorkbookMap) {
		try {
			configWorkbookMap.forEach(excelGenerator::generate);
		} catch (Exception e) {
			logger.error("Error during excel generation!", e);
			throw new EasyExcelException(e);
		}
	}

	public void setExcelConfig(ExcelConfig excelConfig) {
		this.excelConfig = excelConfig;
	}
}
