package com.thecoffee.easyexcel.factory;

class ExcelConfigNullException extends NullPointerException {
	ExcelConfigNullException() {
		super();
	}

	ExcelConfigNullException(String msg) {
		super(msg);
	}
}
