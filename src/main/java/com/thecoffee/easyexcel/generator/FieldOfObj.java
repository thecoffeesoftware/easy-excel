package com.thecoffee.easyexcel.generator;

import java.lang.reflect.Field;

final class FieldOfObj extends Element {

	final Field field;
	private final Object object;
	private final int blockNum;
	private final int order;
	private final ElementType type;
	private final int rowOffset;
	private final int columnOffset;
	private final boolean createHeader;

	FieldOfObj(Field field, Object object, int order, ElementType type, int blockNum, int rowOffset, int columnOffset, boolean createHeader) {
		this.field = field;
		this.object = object;
		this.order = order;
		this.type = type;
		this.blockNum = blockNum;
		this.rowOffset = rowOffset;
		this.columnOffset = columnOffset;
		this.createHeader = createHeader;
	}

	FieldOfObj(Field field, Object object, int order, ElementType type) {
		this(field, object, order, type, 0, 0, 0, false);
	}

	@Override
	int getOrder() {
		return order;
	}

	@Override
	int getRowOffset() {
		return rowOffset;
	}

	@Override
	int getColumnOffset() {
		return columnOffset;
	}

	@Override
	ElementType getType() {
		return type;
	}

	@Override
	int getBlockNum() {
		return blockNum;
	}

	@Override
	Object getObject() {
		return object;
	}

	@Override
	boolean isCreateHeader() {
		return createHeader;
	}
}
