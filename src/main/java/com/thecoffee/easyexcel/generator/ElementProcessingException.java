package com.thecoffee.easyexcel.generator;

class ElementProcessingException extends RuntimeException {
	ElementProcessingException() {
		super();
	}

	ElementProcessingException(String msg) {
		super(msg);
	}

	ElementProcessingException(ReflectiveOperationException e) {
		super(e);
	}
}
