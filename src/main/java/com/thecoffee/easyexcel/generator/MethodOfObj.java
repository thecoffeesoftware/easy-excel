package com.thecoffee.easyexcel.generator;

import java.lang.reflect.Method;

final class MethodOfObj extends Element {

	final Method method;
	private final Object object;
	private final int blockNum;
	private final int order;
	private final ElementType type;
	private final int rowOffset;
	private final int columnOffset;
	private final boolean createHeader;

	MethodOfObj(Method method, Object object, int order, ElementType type, int blockNum, int rowOffset, int columnOffset, boolean createHeader) {
		this.method = method;
		this.object = object;
		this.order = order;
		this.type = type;
		this.blockNum = blockNum;
		this.rowOffset = rowOffset;
		this.columnOffset = columnOffset;
		this.createHeader = createHeader;
	}

	MethodOfObj(Method method, Object workbookObj, int order, ElementType type) {
		this(method, workbookObj, order, type, 0, 0, 0, false);
	}


	@Override
	int getOrder() {
		return order;
	}

	@Override
	int getRowOffset() {
		return rowOffset;
	}

	@Override
	int getColumnOffset() {
		return columnOffset;
	}

	@Override
	ElementType getType() {
		return type;
	}

	@Override
	int getBlockNum() {
		return blockNum;
	}

	@Override
	Object getObject() {
		return object;
	}

	@Override
	boolean isCreateHeader() {
		return createHeader;
	}
}
