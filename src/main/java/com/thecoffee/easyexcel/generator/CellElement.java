package com.thecoffee.easyexcel.generator;

final class CellElement {

	final String fieldName;
	final Object cellObj;
	final int order;

	CellElement(Object cellObj, String fieldName, int order) {
		this.cellObj = cellObj;
		this.fieldName = fieldName;
		this.order = order;
	}

	Object getCellObj() {
		return cellObj;
	}

	String getFieldName() {
		return fieldName;
	}

	int getOrder() {
		return order;
	}
}
