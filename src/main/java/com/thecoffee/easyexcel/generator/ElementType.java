package com.thecoffee.easyexcel.generator;

enum ElementType {
	ROW,
	COLUMN,
	WORKSHEET
}
