package com.thecoffee.easyexcel.generator;

class WorkbookNullException extends NullPointerException {
	WorkbookNullException() {
		super();
	}

	WorkbookNullException(String msg) {
		super(msg);
	}
}
