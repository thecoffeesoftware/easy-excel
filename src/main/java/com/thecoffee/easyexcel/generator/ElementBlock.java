package com.thecoffee.easyexcel.generator;

final class ElementBlock {
	int startCol;
	int startRow;
	int endCol;
	int endRow;

	ElementBlock(int startCol, int startRow, int endCol, int endRow) {
		this.startCol = startCol;
		this.endCol = endCol;
		this.startRow = startRow;
		this.endRow = endRow;
	}
}
