package com.thecoffee.easyexcel.generator;

abstract class Element {
	abstract int getBlockNum();

	abstract Object getObject();

	abstract ElementType getType();

	abstract int getOrder();

	abstract int getRowOffset();

	abstract int getColumnOffset();

	abstract boolean isCreateHeader();
}
