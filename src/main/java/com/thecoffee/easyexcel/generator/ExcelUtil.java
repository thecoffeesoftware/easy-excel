package com.thecoffee.easyexcel.generator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.Date;

final class ExcelUtil {

	private ExcelUtil() {
	}

	static Workbook createWorkbook(boolean useStreaming) {
		if (useStreaming) {
			return new SXSSFWorkbook();
		} else {
			return new XSSFWorkbook();
		}
	}

	static void save(Workbook workbook, OutputStream outputStream) throws IOException {
		workbook.write(outputStream);
		outputStream.close();
	}

	static org.apache.poi.ss.usermodel.Row getOrCreateRow(Sheet worksheet, int rowNum) {
		org.apache.poi.ss.usermodel.Row row = worksheet.getRow(rowNum);
		if (row == null) {
			row = worksheet.createRow(rowNum);
		}
		return row;
	}

	static void setCellValue(Cell cell, Object cellObj) {
		//TODO formula and style
		if (cellObj == null) {
			cell.setCellValue("");
		} else if (cellObj instanceof Date) {
			cell.setCellValue((Date) cellObj);
		} else if (cellObj instanceof LocalDate) {
			cell.setCellValue((LocalDate) cellObj);
		} else if (cellObj instanceof LocalDateTime) {
			cell.setCellValue((LocalDateTime) cellObj);
		} else if (cellObj instanceof Calendar) {
			cell.setCellValue((Calendar) cellObj);
		} else if (cellObj instanceof Number) {
			cell.setCellValue(((Number) cellObj).doubleValue());
		} else if (cellObj instanceof Boolean) {
			cell.setCellValue((Boolean) cellObj);
		} else if (cellObj instanceof RichTextString) {
			cell.setCellValue((RichTextString) cellObj);
		} else if (cellObj instanceof String) {
			cell.setCellValue((String) cellObj);
		} else if (cellObj instanceof OffsetDateTime) {
			cell.setCellValue(((OffsetDateTime) cellObj).toLocalDateTime());
		} else {
			cell.setCellValue(cellObj.toString());
		}
	}
}
