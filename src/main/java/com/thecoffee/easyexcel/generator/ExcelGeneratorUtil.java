package com.thecoffee.easyexcel.generator;

import com.thecoffee.easyexcel.factory.ExcelConfig;
import com.thecoffee.easyexcel.workbook.Column;
import com.thecoffee.easyexcel.workbook.Row;
import com.thecoffee.easyexcel.workbook.Transient;
import com.thecoffee.easyexcel.workbook.TransientElements;
import com.thecoffee.easyexcel.workbook.Workbook;
import com.thecoffee.easyexcel.workbook.Worksheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.thecoffee.easyexcel.generator.ExcelUtil.getOrCreateRow;
import static com.thecoffee.easyexcel.generator.ExcelUtil.setCellValue;
import static com.thecoffee.easyexcel.generator.ReflectionUtil.getAllFields;
import static com.thecoffee.easyexcel.generator.ReflectionUtil.getAllMethods;
import static com.thecoffee.easyexcel.generator.ReflectionUtil.getElementObj;
import static com.thecoffee.easyexcel.generator.ReflectionUtil.getMethodFieldName;

final class ExcelGeneratorUtil {

	private static final String FILE_EXTENSION = ".xlsx";

	private ExcelGeneratorUtil() {
	}


	static String createSheetName(org.apache.poi.ss.usermodel.Workbook workbook, String sheetName) {
		return createSheetName(workbook, sheetName, 0);
	}

	private static String createSheetName(org.apache.poi.ss.usermodel.Workbook workbook, String sheetName, int index) {
		if (workbook.getSheet(sheetName) == null) {
			return sheetName;
		} else {
			if (index > 0) {
				sheetName = sheetName.substring(0, sheetName.length() - Integer.toString(index).length() - 2);
			}
			index++;
			return createSheetName(workbook, sheetName + "(" + index + ")", index);
		}
	}

	static List<Element> getWorkSheetDeclarations(ExcelConfig excelConfig, String name, Object workbookObj) {
		List<Element> worksheetDeclarations = new ArrayList<>();
		worksheetDeclarations.addAll(getSheetFields(excelConfig, workbookObj, name));
		worksheetDeclarations.addAll(getSheetMethods(excelConfig, workbookObj, name));
		return worksheetDeclarations.stream()
				.sorted(Comparator.comparingInt(Element::getOrder))
				.collect(Collectors.toList());
	}

	static List<Element> getElementDeclarationsInOrder(ExcelConfig excelConfig, Object worksheetObj, String workbookName, String sheetName) {
		List<Element> worksheetDeclarations = new ArrayList<>();
		worksheetDeclarations.addAll(getRowFields(excelConfig, worksheetObj, workbookName, sheetName));
		worksheetDeclarations.addAll(getRowMethods(excelConfig, worksheetObj, workbookName, sheetName));
		worksheetDeclarations.addAll(getColumnFields(excelConfig, worksheetObj, workbookName, sheetName));
		worksheetDeclarations.addAll(getColumnMethods(excelConfig, worksheetObj, workbookName, sheetName));
		return worksheetDeclarations.stream()
				.sorted(Comparator.comparingInt(Element::getBlockNum)
						.thenComparingInt(Element::getOrder))
				.collect(Collectors.toList());
	}

	static List<MethodOfObj> getRowMethods(ExcelConfig excelConfig, Object worksheetObj, String workbookName, String sheetName) {
		return getAllMethods(worksheetObj).stream()
				.filter(method -> shouldProcessRow(excelConfig, worksheetObj, workbookName, sheetName, method, method.getName(), true))
				.map(method -> new MethodOfObj(method,
						worksheetObj,
						getOr(method, Row.class, Row::order, 0),
						ElementType.ROW,
						getOr(method, Row.class, Row::blockNum, 0),
						getOr(method, Row.class, Row::rowOffset, 0),
						getOr(method, Row.class, Row::columnOffset, 0),
						getOr(method, Row.class, Row::createHeader, true)))
				.collect(Collectors.toList());
	}

	static List<FieldOfObj> getRowFields(ExcelConfig excelConfig, Object workbookObj, String workbookName, String sheetName) {
		return getAllFields(workbookObj).stream()
				.filter(field -> shouldProcessRow(excelConfig, workbookObj, workbookName, sheetName, field, field.getName(), false))
				.map(field -> new FieldOfObj(field,
						workbookObj,
						getOr(field, Row.class, Row::order, 0),
						ElementType.ROW,
						getOr(field, Row.class, Row::blockNum, 0),
						getOr(field, Row.class, Row::rowOffset, 0),
						getOr(field, Row.class, Row::columnOffset, 0),
						getOr(field, Row.class, Row::createHeader, true)))
				.collect(Collectors.toList());
	}

	private static boolean shouldProcessRow(ExcelConfig excelConfig, Object worksheetObj, String workbookName, String sheetName, AnnotatedElement annotatedElement, String elementName, boolean isMethod) {
		return (excelConfig.includeElementsWithoutAnnotations() && excelConfig.handleElementsWithoutAnnotationsAsRows() ||
				(annotatedElement.isAnnotationPresent(Row.class) &&
						!(Arrays.asList(annotatedElement.getAnnotation(Row.class).transientInWorksheets()).contains(sheetName) ||
								Arrays.asList(annotatedElement.getAnnotation(Row.class).transientInWorkbooks()).contains(workbookName)))
		) && isNotTransient(worksheetObj, annotatedElement, elementName, isMethod);
	}

	private static boolean shouldProcessColumn(ExcelConfig excelConfig, Object worksheetObj, String workbookName, String sheetName, AnnotatedElement annotatedElement, String elementName, boolean isMethod) {
		return (excelConfig.includeElementsWithoutAnnotations() && !excelConfig.handleElementsWithoutAnnotationsAsRows() ||
				(annotatedElement.isAnnotationPresent(Column.class) &&
						!(Arrays.asList(annotatedElement.getAnnotation(Column.class).transientInWorksheets()).contains(sheetName) ||
								Arrays.asList(annotatedElement.getAnnotation(Column.class).transientInWorkbooks()).contains(workbookName)))
		) && isNotTransient(worksheetObj, annotatedElement, elementName, isMethod);
	}

	private static boolean shouldProcessWorksheet(ExcelConfig excelConfig, Object workbookObj, String workbookName, AnnotatedElement annotatedElement, String elementName, boolean isMethod) {
		return (excelConfig.includeElementsWithoutAnnotations() ||
				(annotatedElement.isAnnotationPresent(Worksheet.class) &&
						!Arrays.asList(annotatedElement.getAnnotation(Worksheet.class).transientInWorkbooks()).contains(workbookName))
		) && isNotTransient(workbookObj, annotatedElement, elementName, isMethod);
	}

	private static boolean isCellIncluded(ExcelConfig excelConfig, Object element, String workbookName, String sheetName, AnnotatedElement annotatedElement, String elementName, boolean isMethod) {
		return (excelConfig.includeElementsWithoutAnnotations() ||
				(annotatedElement.isAnnotationPresent(com.thecoffee.easyexcel.workbook.Cell.class) &&
						!(Arrays.asList(annotatedElement.getAnnotation(com.thecoffee.easyexcel.workbook.Cell.class).transientInWorksheets()).contains(sheetName) ||
								Arrays.asList(annotatedElement.getAnnotation(com.thecoffee.easyexcel.workbook.Cell.class).transientInWorkbooks()).contains(workbookName)))
		) && isNotTransient(element, annotatedElement, elementName, isMethod);
	}

	private static boolean isNotTransient(Object parent, AnnotatedElement annotatedElement, String elementName, boolean isMethod) {
		return !annotatedElement.isAnnotationPresent(Transient.class) &&
				(!parent.getClass().isAnnotationPresent(TransientElements.class) ||
						!((isMethod && Arrays.asList(parent.getClass().getAnnotation(TransientElements.class).methods()).contains(elementName)) ||
								Arrays.asList(parent.getClass().getAnnotation(TransientElements.class).fields()).contains(elementName)));
	}

	static List<MethodOfObj> getColumnMethods(ExcelConfig excelConfig, Object worksheetObj, String workbookName, String sheetName) {
		return getAllMethods(worksheetObj).stream()
				.filter(method -> shouldProcessColumn(excelConfig, worksheetObj, workbookName, sheetName, method, method.getName(), true))
				.map(method -> new MethodOfObj(method,
						worksheetObj,
						getOr(method, Column.class, Column::order, 0),
						ElementType.COLUMN,
						getOr(method, Column.class, Column::blockNum, 0),
						getOr(method, Column.class, Column::rowOffset, 0),
						getOr(method, Column.class, Column::columnOffset, 0),
						getOr(method, Column.class, Column::createHeader, true)))
				.collect(Collectors.toList());
	}

	static List<FieldOfObj> getColumnFields(ExcelConfig excelConfig, Object workbookObj, String workbookName, String sheetName) {
		return getAllFields(workbookObj).stream()
				.filter(field -> shouldProcessColumn(excelConfig, workbookObj, workbookName, sheetName, field, field.getName(), false))
				.map(field -> new FieldOfObj(field,
						workbookObj,
						getOr(field, Column.class, Column::order, 0),
						ElementType.COLUMN,
						getOr(field, Column.class, Column::blockNum, 0),
						getOr(field, Column.class, Column::rowOffset, 0),
						getOr(field, Column.class, Column::columnOffset, 0),
						getOr(field, Column.class, Column::createHeader, true)
				))
				.collect(Collectors.toList());
	}

	private static <T> int getOr(AnnotatedElement annotatedElement, Class<? extends Annotation> annotationClass, Function<T, Integer> getter, int defaultVal) {
		return annotatedElement.isAnnotationPresent(annotationClass) ? getter.apply((T) annotatedElement.getAnnotation(annotationClass)) : defaultVal;
	}

	private static <T> boolean getOr(AnnotatedElement annotatedElement, Class<? extends Annotation> annotationClass, Function<T, Boolean> getter, boolean defaultVal) {
		return annotatedElement.isAnnotationPresent(annotationClass) ? getter.apply((T) annotatedElement.getAnnotation(annotationClass)) : defaultVal;
	}

	private static <T> String getOr(AnnotatedElement annotatedElement, Class<? extends Annotation> annotationClass, Function<T, String> getter, String defaultVal) {
		String value = annotatedElement.isAnnotationPresent(annotationClass) ? getter.apply((T) annotatedElement.getAnnotation(annotationClass)) : defaultVal;
		return value.equals("") ? defaultVal : value;
	}

	static List<MethodOfObj> getSheetMethods(ExcelConfig excelConfig, Object workbookObj, String workbookName) {
		return getAllMethods(workbookObj).stream()
				.filter(method -> shouldProcessWorksheet(excelConfig, workbookObj, workbookName, method, method.getName(), true))
				.map(method -> new MethodOfObj(method,
						workbookObj,
						method.isAnnotationPresent(Worksheet.class) ? method.getAnnotation(Worksheet.class).order() : 0,
						ElementType.WORKSHEET))
				.collect(Collectors.toList());
	}

	static List<FieldOfObj> getSheetFields(ExcelConfig excelConfig, Object workbookObj, String workbookName) {
		return getAllFields(workbookObj).stream()
				.filter(field -> shouldProcessWorksheet(excelConfig, workbookObj, workbookName, field, field.getName(), false))
				.map(field -> new FieldOfObj(field,
						workbookObj,
						field.isAnnotationPresent(Worksheet.class) ? field.getAnnotation(Worksheet.class).order() : 0,
						ElementType.WORKSHEET))
				.collect(Collectors.toList());
	}

	static boolean useStreaming(ExcelConfig excelConfig, Workbook workbookAnnotation) {
		boolean useStreaming = false;
		if (excelConfig.useStreaming() != null) {
			useStreaming = excelConfig.useStreaming();
		} else if (workbookAnnotation != null) {
			useStreaming = workbookAnnotation.useStreaming();
		}
		return useStreaming;
	}

	static String getSheetName(Worksheet worksheetAnnotation, String sheetName) {
		if (worksheetAnnotation != null) {
			//later other sheet params can be calculated here
			if (!worksheetAnnotation.name().equals("")) {
				sheetName = worksheetAnnotation.name();
			}
		}
		return sheetName;
	}

	static String getFilename(ExcelConfig excelConfig, String name) {
		if (excelConfig.reportPackage() != null && !excelConfig.reportPackage().equals("")) {
			return excelConfig.reportPackage() + "/" + name + FILE_EXTENSION;
		}
		return name + FILE_EXTENSION;
	}

	static void saveElement(ExcelConfig excelConfig, Sheet worksheet, Element element, Map<Integer, ElementBlock> elementBlockMap, String workbookName, String sheetName) {
		try {
			Object dataGroup = getElementObj(element);
			BlockProcessingInfo blockProcessingInfo = initializeDataProcessing(element, elementBlockMap);
			if (dataGroup instanceof Stream) {
				if (((Stream<?>) dataGroup).count() > 0) {
					if (element.isCreateHeader()) {
						Object data = ((Stream<?>) dataGroup).findFirst().get();
						createHeaders(excelConfig, worksheet, element, workbookName, sheetName, blockProcessingInfo, data);
					}
					((Stream<?>) dataGroup).forEach(data -> saveCells(worksheet,
							element,
							blockProcessingInfo,
							getCells(excelConfig, data, workbookName, sheetName)));
				}
			}
			if (dataGroup instanceof Collection) {
				if (((Collection<?>) dataGroup).size() > 0) {
					if (element.isCreateHeader()) {
						Object data = ((Collection<?>) dataGroup).iterator().next();
						createHeaders(excelConfig, worksheet, element, workbookName, sheetName, blockProcessingInfo, data);
					}
					((Collection<?>) dataGroup).forEach(data -> saveCells(worksheet,
							element,
							blockProcessingInfo,
							getCells(excelConfig, data, workbookName, sheetName)));
				}
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new ElementProcessingException(e);
		}
	}

	private static void createHeaders(ExcelConfig excelConfig, Sheet worksheet, Element element, String workbookName, String sheetName, BlockProcessingInfo blockProcessingInfo, Object data) {
		List<CellElement> cellElements = getCells(excelConfig, data, workbookName, sheetName);
		if (ElementType.ROW.equals(element.getType())) {
			createRowHeaders(worksheet, blockProcessingInfo, cellElements);
		} else {
			createColumnHeaders(worksheet, blockProcessingInfo, cellElements);
		}
	}

	private static void createColumnHeaders(Sheet worksheet, BlockProcessingInfo blockProcessingInfo, List<CellElement> cellElements) {
		int row = blockProcessingInfo.row;
		List<String> headers = cellElements.stream().map(CellElement::getFieldName).collect(Collectors.toList());
		for (String header : headers) {
			org.apache.poi.ss.usermodel.Row currentRow = getOrCreateRow(worksheet, row);
			if (blockProcessingInfo.block.endRow < row) {
				blockProcessingInfo.block.endRow = row;
			}
			Cell cell = currentRow.createCell(blockProcessingInfo.col);
			cell.setCellValue(header);
			row++;
		}
		blockProcessingInfo.col += 1;
	}

	private static void createRowHeaders(Sheet worksheet, BlockProcessingInfo blockProcessingInfo, List<CellElement> cellElements) {
		org.apache.poi.ss.usermodel.Row currentRow = getOrCreateRow(worksheet, blockProcessingInfo.row);
		int col = blockProcessingInfo.col;
		List<String> headers = cellElements.stream().map(CellElement::getFieldName).collect(Collectors.toList());
		for (String header : headers) {
			if (blockProcessingInfo.block.endCol < col) {
				blockProcessingInfo.block.endCol = col;
			}
			Cell cell = currentRow.createCell(col);
			cell.setCellValue(header);
			col++;
		}
		blockProcessingInfo.row += 1;
	}

	static List<CellElement> getCells(ExcelConfig excelConfig, Object data, String workbookName, String sheetName) {
		List<CellElement> cellElements = new ArrayList<>();
		cellElements.addAll(getAllMethods(data).stream()
				.filter(method -> isCellIncluded(excelConfig, data, workbookName, sheetName, method, method.getName(), true))
				.map(method -> {
							try {
								Object cellObj = method.invoke(data);
								return new CellElement(cellObj,
										getOr(method, com.thecoffee.easyexcel.workbook.Cell.class, com.thecoffee.easyexcel.workbook.Cell::headerName, getMethodFieldName(method)),
										getOr(method, com.thecoffee.easyexcel.workbook.Cell.class, com.thecoffee.easyexcel.workbook.Cell::order, 0));
							} catch (IllegalAccessException | InvocationTargetException e) {
								throw new ElementProcessingException("Cell object could not be processed!");
							}
						}
				)
				.collect(Collectors.toList()));
		cellElements.addAll(getAllFields(data).stream()
				.filter(field -> isCellIncluded(excelConfig, data, workbookName, sheetName, field, field.getName(), false))
				.map(field -> {
					try {
						Object cellObj = field.get(data);
						return new CellElement(cellObj,
								getOr(field, com.thecoffee.easyexcel.workbook.Cell.class, com.thecoffee.easyexcel.workbook.Cell::headerName, field.getName()),
								getOr(field, com.thecoffee.easyexcel.workbook.Cell.class, com.thecoffee.easyexcel.workbook.Cell::order, 0));
					} catch (IllegalAccessException e) {
						throw new ElementProcessingException("Cell object could not be processed!");
					}
				})
				.collect(Collectors.toList()));
		return cellElements.stream().sorted(Comparator.comparingInt(CellElement::getOrder)).collect(Collectors.toList());
	}

	private static BlockProcessingInfo initializeDataProcessing(Element element, Map<Integer, ElementBlock> elementBlockMap) {
		int col = 0, row = 0;
		ElementBlock currentBlock = null;
		if (elementBlockMap.get(element.getBlockNum()) == null) {
			int currentBlockNum = element.getBlockNum() - 1;
			while (currentBlockNum >= 0) {
				if (elementBlockMap.get(currentBlockNum) != null) {
					col = elementBlockMap.get(currentBlockNum).endCol;
					currentBlock = new ElementBlock(col, row, row, col);
					elementBlockMap.put(element.getBlockNum(), currentBlock);
					break;
				}
				currentBlockNum--;
			}
		} else {
			currentBlock = elementBlockMap.get(element.getBlockNum());
			row = elementBlockMap.get(element.getBlockNum()).endRow;
			col = elementBlockMap.get(element.getBlockNum()).startCol;
		}
		if (currentBlock == null) {
			currentBlock = new ElementBlock(col, row, row, col);
			elementBlockMap.put(element.getBlockNum(), currentBlock);
		}
		col += element.getColumnOffset();
		row += element.getRowOffset();
		return new BlockProcessingInfo(currentBlock, col, row);
	}

	private static void saveCells(Sheet worksheet, Element element, BlockProcessingInfo blockProcessingInfo, List<CellElement> cellElements) {
		int rowNum = blockProcessingInfo.row;
		int columnNum = blockProcessingInfo.col;
		if (ElementType.ROW.equals(element.getType())) {
			org.apache.poi.ss.usermodel.Row row = getOrCreateRow(worksheet, rowNum);
			for (CellElement cellElement : cellElements) {
				Cell cell = row.createCell(columnNum);
				setCellValue(cell, cellElement.getCellObj());
				columnNum++;
			}
			rowNum++;
			blockProcessingInfo.row = rowNum;
			setEndLimitsOfBlock(blockProcessingInfo, rowNum, columnNum);
		} else if (ElementType.COLUMN.equals(element.getType())) {
			for (CellElement cellElement : cellElements) {
				org.apache.poi.ss.usermodel.Row row = getOrCreateRow(worksheet, rowNum);
				Cell cell = row.createCell(columnNum);
				setCellValue(cell, cellElement.getCellObj());
				rowNum++;
			}
			columnNum++;
			blockProcessingInfo.col = columnNum;
			setEndLimitsOfBlock(blockProcessingInfo, rowNum, columnNum);
		} else {
			throw new ElementProcessingException("Unknown element type!");
		}
	}

	private static void setEndLimitsOfBlock(BlockProcessingInfo blockProcessingInfo, int rowNum, int columnNum) {
		if (columnNum > blockProcessingInfo.block.endCol) {
			blockProcessingInfo.block.endCol = columnNum;
		}
		if (rowNum > blockProcessingInfo.block.endRow) {
			blockProcessingInfo.block.endRow = rowNum;
		}
	}

}
