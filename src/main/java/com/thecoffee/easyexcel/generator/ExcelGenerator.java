package com.thecoffee.easyexcel.generator;

import com.thecoffee.easyexcel.factory.DefaultExcelConfig;
import com.thecoffee.easyexcel.factory.ExcelConfig;
import com.thecoffee.easyexcel.workbook.Workbook;
import com.thecoffee.easyexcel.workbook.Worksheet;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.thecoffee.easyexcel.generator.ExcelGeneratorUtil.createSheetName;
import static com.thecoffee.easyexcel.generator.ExcelGeneratorUtil.getElementDeclarationsInOrder;
import static com.thecoffee.easyexcel.generator.ExcelGeneratorUtil.getFilename;
import static com.thecoffee.easyexcel.generator.ExcelGeneratorUtil.getSheetName;
import static com.thecoffee.easyexcel.generator.ExcelGeneratorUtil.getWorkSheetDeclarations;
import static com.thecoffee.easyexcel.generator.ExcelGeneratorUtil.useStreaming;

public final class ExcelGenerator {

	private static ExcelGenerator excelGenerator;
	private final Logger logger = LoggerFactory.getLogger(ExcelGenerator.class);

	private ExcelGenerator() {
	}

	public static ExcelGenerator getInstance() {
		if (excelGenerator == null) {
			synchronized (ExcelGenerator.class) {
				if (excelGenerator == null) {
					excelGenerator = new ExcelGenerator();
				}
			}
		}
		return excelGenerator;
	}

	public void generate(Object workbook) {
		generate(DefaultExcelConfig.getInstance(), workbook);
	}

	public void generate(ExcelConfig excelConfig, Object workbook) {
		if (workbook == null) {
			throw new WorkbookNullException("Workbook cannot be null!");
		}
		Workbook workbookAnnotation = workbook.getClass().getAnnotation(Workbook.class);
		String name = workbook.getClass().getSimpleName();
		if (workbookAnnotation != null && !workbookAnnotation.name().equals("")) {
			name = workbookAnnotation.name();
		}
		generate(excelConfig, name, workbook);
	}

	public void generate(String name, Object workbook) {
		generate(DefaultExcelConfig.getInstance(), name, workbook);
	}

	public void generate(ExcelConfig excelConfig, String name, Object workbookObj) {
		logger.info("Workbook generation started!");
		if (workbookObj == null) {
			throw new WorkbookNullException("Workbook cannot be null!");
		}

		Workbook workbookAnnotation = workbookObj.getClass().getAnnotation(Workbook.class);
		List<Element> worksheetDeclarations = getWorkSheetDeclarations(excelConfig, name, workbookObj);

		boolean useStreaming = useStreaming(excelConfig, workbookAnnotation);
		File reportPackage = new File(excelConfig.reportPackage());
		if(!reportPackage.exists()){
			//noinspection ResultOfMethodCallIgnored
			reportPackage.mkdirs();
		}
		try (OutputStream fileOut = new FileOutputStream(getFilename(excelConfig, name))) {
			org.apache.poi.ss.usermodel.Workbook workbook = ExcelUtil.createWorkbook(useStreaming);
			createWorksheets(excelConfig, name, worksheetDeclarations, workbook);
			ExcelUtil.save(workbook, fileOut);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		logger.info("Workbook generation finished!");
	}

	private void createWorksheets(ExcelConfig excelConfig, String name, List<Element> worksheetDeclarations, org.apache.poi.ss.usermodel.Workbook workbook) {
		worksheetDeclarations.forEach(worksheetDeclaration -> {
			if (worksheetDeclaration.getClass().isAssignableFrom(FieldOfObj.class)) {
				createWorksheetFromField(excelConfig, workbook, (FieldOfObj) worksheetDeclaration, name);
			} else if (worksheetDeclaration.getClass().isAssignableFrom(MethodOfObj.class)) {
				createWorksheetFromMethod(excelConfig, workbook, (MethodOfObj) worksheetDeclaration, name);
			}
		});
	}

	private void createWorksheetFromField(ExcelConfig excelConfig, org.apache.poi.ss.usermodel.Workbook workbook, FieldOfObj fieldOfObj, String workbookName) {
		try {
			Worksheet worksheetAnnotation = fieldOfObj.field.getAnnotation(Worksheet.class);
			String sheetName = fieldOfObj.field.getName();
			sheetName = createSheetName(workbook, getSheetName(worksheetAnnotation, sheetName));
			Sheet worksheet = workbook.createSheet(sheetName);
			Object worksheetObj = fieldOfObj.field.get(fieldOfObj.getObject());
			if (worksheetObj != null) {
				createWorkSheetFromObject(excelConfig, worksheet, worksheetObj, workbookName, sheetName);
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	private void createWorksheetFromMethod(ExcelConfig excelConfig, org.apache.poi.ss.usermodel.Workbook workbook, MethodOfObj methodOfObj, String workbookName) {
		try {
			Worksheet worksheetAnnotation = methodOfObj.method.getAnnotation(Worksheet.class);
			String sheetName = methodOfObj.method.getName();
			if (sheetName.startsWith("get")) {
				sheetName = sheetName.substring(3);
			}
			sheetName = createSheetName(workbook, getSheetName(worksheetAnnotation, sheetName));
			Object worksheetObj = methodOfObj.method.invoke(methodOfObj.getObject());
			if (worksheetObj != null) {
				Sheet worksheet = workbook.createSheet(sheetName);
				createWorkSheetFromObject(excelConfig, worksheet, worksheetObj, workbookName, sheetName);
			}
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private void createWorkSheetFromObject(ExcelConfig excelConfig, Sheet worksheet, Object worksheetObj, String workbookName, String sheetName) {
		List<Element> elementDeclarations = getElementDeclarationsInOrder(excelConfig, worksheetObj, workbookName, sheetName);
		Map<Integer, ElementBlock> elementBlockMap = new HashMap<>();
		elementDeclarations.forEach(element -> ExcelGeneratorUtil.saveElement(excelConfig, worksheet, element, elementBlockMap, workbookName, sheetName));
	}

}
