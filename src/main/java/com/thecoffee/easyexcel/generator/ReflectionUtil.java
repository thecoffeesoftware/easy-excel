package com.thecoffee.easyexcel.generator;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

final class ReflectionUtil {

	static final List<String> objectMethods = Arrays.stream(Object.class.getDeclaredMethods()).map(Method::getName).collect(Collectors.toList());
	static final String LOMBOK_DEFAULT = "$default$";

	static {
		objectMethods.add("builder");
	}

	static List<Field> getAllFields(Object obj) {
		List<Field> fields = new ArrayList<>();
		Class<?> clazz = obj.getClass();
		while (clazz != Object.class) {
			fields.addAll(Arrays.stream(clazz.getDeclaredFields())
					.filter(field -> !field.getName().startsWith(LOMBOK_DEFAULT))
					.collect(Collectors.toList()));
			clazz = clazz.getSuperclass();
		}
		fields.forEach(field -> field.setAccessible(true));
		return fields;
	}

	static List<Method> getAllMethods(Object obj) {
		List<Method> methods = new ArrayList<>();
		Class<?> clazz = obj.getClass();
		while (clazz != Object.class) {
			List<String> fieldNames = getAllFields(obj).stream().map(Field::getName).collect(Collectors.toList());
			methods.addAll(Arrays.stream(clazz.getDeclaredMethods())
					.filter(method -> !objectMethods.contains(method.getName()) &&
							!method.getReturnType().equals(Void.TYPE) &&
							method.getParameterCount() == 0 &&
							fieldNames.stream().noneMatch(fieldName -> ("get" + capitalize(fieldName)).equals(method.getName())) &&
							!method.getName().startsWith(LOMBOK_DEFAULT))
					.collect(Collectors.toList()));
			clazz = clazz.getSuperclass();
		}
		methods.forEach(method -> method.setAccessible(true));
		return methods;
	}

	private static String capitalize(String string) {
		return Character.toUpperCase(string.charAt(0)) + string.substring(1);
	}

	static String getMethodFieldName(Method method) {
		String fieldName = method.getName();
		if (fieldName.startsWith("get")) {
			fieldName = Character.toLowerCase(fieldName.charAt(3)) + fieldName.substring(4);
		}
		return fieldName;
	}

	static Object getElementObj(Element element) throws IllegalAccessException, InvocationTargetException {
		Object elementObj;
		if (element instanceof MethodOfObj) {
			elementObj = ((MethodOfObj) element).method.invoke(element.getObject());
		} else if (element instanceof FieldOfObj) {
			elementObj = ((FieldOfObj) element).field.get(element.getObject());
		} else {
			throw new ElementProcessingException("Unknown element group was defined!");
		}
		return elementObj;
	}

}
