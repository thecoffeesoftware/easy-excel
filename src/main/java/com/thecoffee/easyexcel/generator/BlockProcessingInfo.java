package com.thecoffee.easyexcel.generator;

class BlockProcessingInfo {
	ElementBlock block;
	int col;
	int row;

	public BlockProcessingInfo(ElementBlock block, int col, int row) {
		this.block = block;
		this.col = col;
		this.row = row;
	}
}
